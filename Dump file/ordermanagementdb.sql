/*
 Navicat MySQL Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100414
 Source Host           : localhost:3306
 Source Schema         : ordermanagementdb

 Target Server Type    : MySQL
 Target Server Version : 100414
 File Encoding         : 65001

 Date: 20/05/2021 23:58:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for client
-- ----------------------------
DROP TABLE IF EXISTS `client`;
CREATE TABLE `client`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `age` int NOT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `town` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_client_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of client
-- ----------------------------
INSERT INTO `client` VALUES (1, 'Marcu Fred', 25, 'marcufred@yahoo.com', 'Pitesti');
INSERT INTO `client` VALUES (2, 'Fred Andreea', 41, 'frandreea@gmail.com', 'Cluj');
INSERT INTO `client` VALUES (3, 'Lort Alex', 25, 'lortalex@yahoo.com', 'Covasna');
INSERT INTO `client` VALUES (4, 'Gonzalez Marc', 21, 'marcgon@gmail.com', 'Covasna');
INSERT INTO `client` VALUES (5, 'Mast Rares', 24, 'mastrares@gmail.com', 'Arad');
INSERT INTO `client` VALUES (6, 'Fort Maria', 22, 'fmaria@gmail.com', 'Cluj');
INSERT INTO `client` VALUES (7, 'Drepo Mara', 21, 'drepomara@yahoo.com', 'Bucuresti');
INSERT INTO `client` VALUES (8, 'Marc Alexandra', 35, 'marcalexandra@yahoo.com', 'Iasi');
INSERT INTO `client` VALUES (9, 'Popescu Andrei', 21, 'popescuandrei@yahoo.com', 'Craiova');
INSERT INTO `client` VALUES (10, 'Dumiter Alexandra', 23, 'dumiteralexandra@gmail.com', 'Timisoara');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `productName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `clientName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `quantity` int NOT NULL,
  `price` int NOT NULL,
  `shippingCost` int NULL DEFAULT NULL,
  `total` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `client_foreign_idx`(`clientName`) USING BTREE,
  INDEX `product_foreign_idx`(`productName`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (0, 'Apa Azuga 5L', 'Marcu Fred', 1, 5, 15, 20);
INSERT INTO `orders` VALUES (1, 'Shimano XT set', 'Pop Alex', 1, 1200, 0, 1200);
INSERT INTO `orders` VALUES (2, 'Apa Azuga 5L', 'Marcu Fred', 3, 5, 15, 30);
INSERT INTO `orders` VALUES (3, 'Apa Azuga 5L', 'Andreescu Andreea', 2, 5, 15, 25);
INSERT INTO `orders` VALUES (4, 'Apa Azuga 5L', 'Gonzalez Marc', 2, 5, 15, 25);
INSERT INTO `orders` VALUES (5, 'Apa Azuga 5L', 'Lort Alex', 2, 5, 15, 25);
INSERT INTO `orders` VALUES (6, 'Apa Azuga 5L', 'Lort Alex', 1, 5, 15, 20);
INSERT INTO `orders` VALUES (7, 'Apa Azuga 5L', 'Marcu Fred', 1, 5, 15, 20);
INSERT INTO `orders` VALUES (8, 'Alpina Thunder', 'Gonzalez Marc', 2, 300, 15, 615);
INSERT INTO `orders` VALUES (9, 'Apa Dorna 5L', 'Marcu Fred', 1, 4, 15, 19);
INSERT INTO `orders` VALUES (10, 'Apa Dorna 5L', 'Marcu Fred', 1, 4, 15, 19);
INSERT INTO `orders` VALUES (11, 'Shimano SLX set', 'Andreescu Andreea', 1, 800, 0, 800);
INSERT INTO `orders` VALUES (12, 'Apa Azuga 5L', 'Andreescu Andreea', 2, 5, 15, 25);
INSERT INTO `orders` VALUES (13, 'Apa Azuga 5L', 'Lort Alex', 2, 5, 15, 25);
INSERT INTO `orders` VALUES (14, 'Shimano XT set', 'Drepo Mara', 2, 1200, 0, 2400);
INSERT INTO `orders` VALUES (15, 'Shimano XT set', 'Drepo Mara', 2, 1200, 0, 2400);
INSERT INTO `orders` VALUES (16, 'Apa Azuga 5L', 'Fort Maria', 1, 5, 15, 20);
INSERT INTO `orders` VALUES (17, 'Shimano SLX set', 'Andreescu Andreea', 2, 800, 0, 1600);
INSERT INTO `orders` VALUES (18, 'Shimano XT set', 'Marc Alexandra', 2, 1200, 0, 2400);
INSERT INTO `orders` VALUES (19, 'Slipknot Shirt', 'Lort Alex', 1, 50, 15, 65);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `category` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `quantity` int NULL DEFAULT NULL,
  `price` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (1, 'Shimano SLX set', 'Bike Brakes', 7, 800);
INSERT INTO `product` VALUES (2, 'Shimano XT set', 'Bike Brakes', 6, 1200);
INSERT INTO `product` VALUES (3, 'Alpina Thunder', 'Bike Helmets', 11, 300);
INSERT INTO `product` VALUES (4, 'Apa Azuga 5L', 'Bauturi', 23, 5);
INSERT INTO `product` VALUES (5, 'Slipknot Shirt', 'Clothes', 3, 50);
INSERT INTO `product` VALUES (6, 'FIFA 21', 'Games', 30, 150);
INSERT INTO `product` VALUES (7, 'Light Set', 'Bike', 14, 80);

SET FOREIGN_KEY_CHECKS = 1;

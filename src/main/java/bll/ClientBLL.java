package bll;

import bll.validators.AgeValidator;
import bll.validators.EmailValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import model.Client;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Class used to make the specific queries from AbstractDao for the Client type
 */

public class ClientBLL {
    private List<Validator<Client>> validators;
    private ClientDAO clientDAO;

    public ClientBLL(){
        validators = new ArrayList<Validator<Client>>();

        validators.add(new AgeValidator());
        validators.add(new EmailValidator());

        clientDAO = new ClientDAO();
    }
    public void updateClient(Client client){
        validators.get(0).validate(client);
        validators.get(1).validate(client);
        clientDAO.update(client);
    }
    public void insertClient(Client client){
        validators.get(0).validate(client);
        validators.get(1).validate(client);
        clientDAO.insert(client);
    }
    public void deleteClient(int id){
        clientDAO.delete(id);
    }
    public List<Client> findAllClients(){
        List<Client> clients = clientDAO.findAll();
        if(clients == null){
            throw new NoSuchElementException("There are no clients");
        }
        return clients;
    }

    /**
     * Method used to create the table columns for view all operations, after they
     * were extracted by reflection in the createTableHeader method and populate the table with the data
     * @param table the table which we want to make the columns
     * @return the table with the columns updated
     */
    public TableView<Client> getTableHeaders(TableView<Client> table,List<Client> clients,int initializeTable){
        TableView<Client> clientTableView;
        if(initializeTable == 0) {
            List<String> headers = clientDAO.createTableHeader();
            for (String string : headers) {
                TableColumn<Client, String> tableColumn = new TableColumn<>(string);
                table.getColumns().add(tableColumn);
            }
        }
        for(Client client:clients){
            table.getItems().add(client);
        }
        clientTableView = table;
        return  clientTableView;
    }

    public Client findClientById(int id){
        Client client = clientDAO.findById(id);
        if(client == null){
            throw new NoSuchElementException("The client with id: " + id + " was not found");
        }
        return client;
    }
}

package bll;

import bll.validators.ProductPriceValidator;
import bll.validators.ProductQuantityValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import model.Client;
import model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Class used to make the specific queries from AbstractDao for the Product type
 */

public class ProductBLL {
    private List<Validator<Product>> validators;
    private ProductDAO productDAO;

    public ProductBLL(){
        validators = new ArrayList<>();

        validators.add(new ProductQuantityValidator());
        validators.add(new ProductPriceValidator());


        productDAO = new ProductDAO();
    }

    public void deleteProduct(int id){
        productDAO.delete(id);
    }

    public void insertProduct(Product product){
        validators.get(0).validate(product);
        validators.get(1).validate(product);

        productDAO.insert(product);
    }
    public void updateProduct(Product product){
        validators.get(0).validate(product);
        validators.get(1).validate(product);

        productDAO.update(product);
    }

    /**
     * Method used to create the table columns for view all operations, after they
     * were extracted by reflection in the createTableHeader method and populate the table with the data
     * @param table the table which we want to make the columns
     * @return the table with the columns updated
     */
    public TableView<Product> getTableHeaders(TableView<Product> table,List<Product> products, int initializeTable){
        TableView<Product> productTableView;
        if(initializeTable == 0) {
            List<String> headers = productDAO.createTableHeader();
            for (String string : headers) {
                TableColumn<Product, String> tableColumn = new TableColumn<>(string);
                table.getColumns().add(tableColumn);
            }
        }
        for(Product product:products){
            table.getItems().add(product);
        }
        productTableView = table;
        return  productTableView;
    }
    public List<Product> findAllProducts(){
        List<Product> products = productDAO.findAll();
        if(products == null){
            throw new NoSuchElementException("There are no products");
        }
        return products;
    }

    public Product findProductById(int id){
        Product product = productDAO.findById(id);
        if( product == null){
            throw new NoSuchElementException("The product with id: " + id + "doesn't exist");
        }
        return product;
    }
}

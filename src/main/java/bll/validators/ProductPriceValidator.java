package bll.validators;

import model.Product;

/**
 * Class used to validate the price of a product
 */

public class ProductPriceValidator implements Validator<Product> {
    @Override
    public void validate(Product product) {
        if(product.getPrice() <= 0){
            throw new IllegalArgumentException("The price is not valid");
        }
    }
}

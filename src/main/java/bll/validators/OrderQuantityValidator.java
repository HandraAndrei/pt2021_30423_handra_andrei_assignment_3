package bll.validators;

import model.Orders;

/**
 * Class used to validate de quantity of the order
 */

public class OrderQuantityValidator implements Validator<Orders>{
    @Override
    public void validate(Orders orders) {
        if(orders.getQuantity() <= 0){
            throw new IllegalArgumentException("The quantity for the order is not valid");
        }
    }
}

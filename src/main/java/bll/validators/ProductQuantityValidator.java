package bll.validators;

import model.Product;

/**
 * Class used to validate the quantity of a product
 */

public class ProductQuantityValidator implements Validator<Product>{
    @Override
    public void validate(Product product) {
        if(product.getQuantity() < 0){
            throw new IllegalArgumentException("The quantity is not valid");
        }
    }
}

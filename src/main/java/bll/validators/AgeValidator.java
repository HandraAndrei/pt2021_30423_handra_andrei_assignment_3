package bll.validators;

import model.Client;

/**
 * Class used to validate the age of the client
 */
public class AgeValidator implements Validator<Client>{

    private static final int MIN_AGE = 18;
    @Override
    public void validate(Client client) {
        if(client.getAge() < MIN_AGE){
            throw new IllegalArgumentException("The client age limit is not respected");
        }

    }
}

package bll;

import bll.validators.OrderQuantityValidator;
import bll.validators.Validator;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import dao.OrdersDAO;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import model.Orders;
import model.Product;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Class used to make the specific queries from AbstractDao for the Orders type
 */

public class OrdersBLL {
    private List<Validator<Orders>> validators;
    private OrdersDAO ordersDAO;

    public OrdersBLL(){
        validators = new ArrayList<>();

        validators.add(new OrderQuantityValidator());

        ordersDAO = new OrdersDAO();
    }

    /**
     * Method used to create the table columns for view all operations, after they
     * were extracted by reflection in the createTableHeader method and populate the table with the data
     * @param table the table which we want to make the columns
     * @return the table with the columns updated
     */
    public TableView<Orders> getTableHeaders(TableView<Orders> table,List<Orders> orders,int initializeTable){
        TableView<Orders> ordersTableView;
        List<String> headers = ordersDAO.createTableHeader();
        if(initializeTable == 0) {
            for (String string : headers) {
                TableColumn<Orders, String> tableColumn = new TableColumn<>(string);
                table.getColumns().add(tableColumn);
            }
        }
        for (Orders order : orders) {
            table.getItems().add(order);
        }
        ordersTableView = table;
        return ordersTableView;

    }
    public void insertOrder(Orders order){
        validators.get(0).validate(order);

        ordersDAO.insert(order);
    }
    public List<Orders> findAllOrders(){
        List<Orders> orders = ordersDAO.findAll();
        if(orders == null){
            throw new NoSuchElementException("There are no orders");
        }
        return orders;
    }
    public Orders findOrderById(int id){
        Orders order = ordersDAO.findById(id);
        if(order == null){
            throw new NoSuchElementException("The order with id: " + id + " doesn't exist");
        }
        return order;
    }

}

package model;

/**
 * This class represents the Orders class
 * Here are the specific fields for the Orders class
 */

public class Orders {
    private int id;
    private String productName;
    private String clientName;
    private int quantity;
    private int price;
    private int shippingCost;
    private int total;

    public Orders(){

    }

    public Orders(int id, String productName, String clientName, int quantity, int price, int shippingCost, int total) {
        this.id = id;
        this.productName = productName;
        this.clientName = clientName;
        this.quantity = quantity;
        this.price = price;
        this.shippingCost = shippingCost;
        this.total = total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(int shippingCost) {
        this.shippingCost = shippingCost;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}

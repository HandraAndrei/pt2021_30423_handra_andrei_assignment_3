package model;

/**
 * This class represents the Client class
 * Here are the specific fields for the Client class
 */

public class Client {
    private int id;
    private String name;
    private int age;
    private String email;
    private String town;

    public Client(){

    }

    public Client(int id, String name, int age, String email, String town) {
        super();
        this.id = id;
        this.name = name;
        this.age = age;
        this.email = email;
        this.town = town;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }
    @Override
    public String toString() {
        return name +", " + email + ", " + town;
    }

}

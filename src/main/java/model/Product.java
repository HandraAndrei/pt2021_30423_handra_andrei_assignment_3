package model;

/**
 * This class represents the Product class
 * Here are the specific fields for the Product class
 */

public class Product {
    private int id;
    private String name;
    private String category;
    private int quantity;
    private int price;

    public Product() {
    }

    public Product(int id, String name, String category, int quantity, int price) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.quantity = quantity;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString(){
        return name + ", price=" + price + ", quantity=" + quantity;
    }
}

package dao;

import connection.ConnectionFactory;
import javafx.scene.control.TableView;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * In this class are all the specific queries to communicate with the database and
 * the methods used to get the data from the database for that specific queries
 * @param <T> is used for Reflection, which at runtime will get the Class from where it was run
 */

public class AbstractDAO<T> {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T> type;
    @SuppressWarnings("unchecked")
    public AbstractDAO(){
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    }

    private String createSelectQuery(String field){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }
    private String selectAll(){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        return sb.toString();
    }
    private String createDeleteQuery(String field){
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + "=?");
        return sb.toString();
    }
    private String createInsertQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT ");
        sb.append(" INTO ");
        sb.append(type.getSimpleName());
        sb.append(" ( ");
        for(Field field: type.getDeclaredFields()){
            field.setAccessible(true);
            sb.append(field.getName());
            sb.append(" , ");
        }
        sb.setLength(sb.length()-2);
        sb.append(" ) ");
        sb.append(" VALUES (");
        for(Field field: type.getDeclaredFields()){
            field.setAccessible(true);
            sb.append(" ?, ");
        }
        sb.setLength(sb.length()-2);
        sb.append(" ) ");
        return sb.toString();
    }
    private String createUpdateQuery(String field){
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(type.getSimpleName());
        sb.append(" SET ");
        for(Field fields: type.getDeclaredFields()){
            fields.setAccessible(true);
            sb.append(fields.getName());
            sb.append(" = ");
            sb.append(" ?, ");
        }
        sb.setLength(sb.length()-2);
        sb.append(" WHERE " + field + "=?");
        return sb.toString();
    }

    /**
     * This method is used to perfrom the Update query
     * @param t is the object which we want to be updated
     * @return the updated object
     */
    public T update(T t){
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createUpdateQuery("id");
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            int i=1;
            for(Field field:type.getDeclaredFields()){
                field.setAccessible(true);
                statement.setString(i,field.get(t).toString());
                i++;
            }
            for(Field field:type.getDeclaredFields()){
                field.setAccessible(true);
                if(field.getName().equals("id")){
                    statement.setString(i,field.get(t).toString());
                }
            }
            statement.executeUpdate();
        }catch(SQLException e){
            LOGGER.log(Level.WARNING,type.getName()+ "DAO: update "+ e.getMessage());
        }catch(IllegalAccessException e){
            LOGGER.log(Level.WARNING,type.getName() + "DAO: update " +e.getMessage() );
        }
        return null;
    }

    /**
     * This method is used to perfrom the Insert query
     * @param t is the object which we want to insert
     * @return the inserted object
     */
    public T insert(T t){
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createInsertQuery();
        try{
            connection =ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            int i=1;
            for(Field field: type.getDeclaredFields()){
                field.setAccessible(true);
                statement.setString(i,field.get(t).toString());
                i++;
            }
            statement.executeUpdate();

        }catch(SQLException e){
            LOGGER.log(Level.WARNING,type.getName() +"DAO: insert " + e.getMessage());
        }catch(IllegalAccessException e){
            LOGGER.log(Level.WARNING,type.getName() + "DAO: insert " + e.getMessage());
        }
        return null;
    }

    /**
     * This method is used to perform the delete query
     * @param id is the id of the object that we want to delete
     */
    public void delete(int id){
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createDeleteQuery("id");
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1,id);
            statement.executeUpdate();
        }catch (SQLException e){
            LOGGER.log(Level.WARNING,type.getName() + "DAO: delete" + e.getMessage());
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }

    /**
     * Method used for the Select * query
     * @return a list of all the objects of the T type
     */
    public List<T> findAll(){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = selectAll();
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            return createObjects(resultSet);
        }catch(SQLException e){
            LOGGER.log(Level.WARNING,type.getName() + "DAO:findAll" + e.getMessage());
        }finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * This method is used for the Select query
     * @param id the id of the object we want to find
     * @return the found object
     */
    public T findById(int id){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("id");
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1,id);
            resultSet= statement.executeQuery();
            return createObjects(resultSet).get(0);
        }catch(SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        }catch(IndexOutOfBoundsException e){

        }finally{
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * This method use reflection technique to generate the headers of the table
     * @return a list with the table for the View All operation
     */
    public List<String> createTableHeader(){
        List<String> headers = new ArrayList<>();
        for(Field field:type.getDeclaredFields()){
            String fieldName = field.getName();
            headers.add(fieldName);
        }
        return headers;

    }

    /**
     * Method used to create the objects from the query
     * @param resultSet the resultSet from the query
     * @return a list with the objects created
     */
    private List<T> createObjects(ResultSet resultSet){
        List<T> list = new ArrayList<T>();
        Constructor[] constructors = type.getDeclaredConstructors();
        Constructor constructor = null;
        for(int i=0;i<constructors.length;i++){
            constructor = constructors[i];
            if(constructor.getGenericParameterTypes().length == 0){
                break;
            }
        }
        try{
            while(resultSet.next()){
                constructor.setAccessible(true);
                T instance = (T)constructor.newInstance();
                for(Field field: type.getDeclaredFields()){
                    String fieldName = field.getName();
                    Object value = resultSet.getObject(fieldName);
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName,type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance,value);
                }
                list.add(instance);
            }
            }catch (InstantiationException e){
                e.printStackTrace();
            }catch(IllegalAccessException e){
                e.printStackTrace();
            }catch (SecurityException e){
                e.printStackTrace();
            }catch(IllegalArgumentException e){
                e.printStackTrace();
            }catch(InvocationTargetException e){
                e.printStackTrace();
            }catch(SQLException e){
                e.printStackTrace();
            }catch(IntrospectionException e){
                e.printStackTrace();
            }
        return list;
    }
}

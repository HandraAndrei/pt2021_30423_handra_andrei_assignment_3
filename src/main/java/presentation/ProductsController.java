package presentation;

import bll.ClientBLL;
import bll.ProductBLL;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Client;
import model.Product;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * This class is used for managing the Product operations:
 * Add new Product, Edit Product, Delete Product and View all Products
 */

public class ProductsController {

    @FXML
    private Button newProduct;
    @FXML
    private Button editProduct;
    @FXML
    private Button deleteProduct;
    @FXML
    private Button viewProducts;
    @FXML
    private Label idLabel;
    @FXML
    private Label nameLabel;
    @FXML
    private Label categoryLabel;
    @FXML
    private Label quantityLabel;
    @FXML
    private Label priceLabel;
    @FXML
    private TextField id;
    @FXML
    private TextField name;
    @FXML
    private TextField category;
    @FXML
    private TextField quantity;
    @FXML
    private TextField price;
    @FXML
    private Label errorLabel;
    @FXML
    private Button enter;
    @FXML
    private Button clear;
    @FXML
    TableView<Product> productsTable;
    Product product;

    /**
     * These fields are used to know when I press the Enter button, from
     * which operation we came ( The button that was pressed before )
     */

    private int fromDelete = 0;
    private int fromNewProduct = 0;
    private int fromEdit = 0;

    private int initializeTable = 0;
    private int updateError = 0;

    /**
     * In this method the GUI for the Product operations is implemented
     * @param e is used to select between the available buttons to implement the required product operations
     */
    @FXML
    public void onButtonClicked(ActionEvent e){
        if(e.getSource().equals(newProduct)){
            product = new Product();
            fromNewProduct = 1;
            newProduct.setDisable(true);
            editProduct.setDisable(true);
            deleteProduct.setDisable(true);
            viewProducts.setDisable(true);
            idLabel.setVisible(true);
            id.setVisible(true);

        }else if(e.getSource().equals(editProduct)){
            fromEdit = 1;
            newProduct.setDisable(true);
            editProduct.setDisable(true);
            deleteProduct.setDisable(true);
            viewProducts.setDisable(true);
            idLabel.setVisible(true);
            id.setVisible(true);

        }else if(e.getSource().equals(deleteProduct)){
            fromDelete = 1;
            newProduct.setDisable(true);
            editProduct.setDisable(true);
            deleteProduct.setDisable(true);
            viewProducts.setDisable(true);
            idLabel.setVisible(true);
            id.setVisible(true);

        }else if(e.getSource().equals(viewProducts)){
            productsTable.getItems().clear();
            productsTable.setEditable(true);
            ProductBLL productBLL = new ProductBLL();
            List<Product> products = productBLL.findAllProducts();
            if(initializeTable == 0){
                productsTable = productBLL.getTableHeaders(productsTable,products,initializeTable);
                initializeTable = 1;
            }else if(initializeTable == 1){
                productsTable = productBLL.getTableHeaders(productsTable,products,initializeTable);
            }
            productsTable.getColumns().get(0).setPrefWidth(50);
            productsTable.getColumns().get(1).setPrefWidth(200);
            productsTable.getColumns().get(2).setPrefWidth(200);
            productsTable.getColumns().get(3).setPrefWidth(100);
            productsTable.getColumns().get(4).setPrefWidth(100);

            productsTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("id"));
            productsTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("name"));
            productsTable.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("category"));
            productsTable.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("quantity"));
            productsTable.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("price"));
            productsTable.setVisible(true);
        }else if(e.getSource().equals(enter)){
            if(fromDelete == 1){
                int delete = 0;
                if(!id.getText().isEmpty()){
                    try {
                        delete = Integer.parseInt(id.getText());
                        ProductBLL productBLL = new ProductBLL();
                        Product product = null;
                        product = productBLL.findProductById(delete);
                        productBLL.deleteProduct(delete);
                        errorLabel.setText("The product with id " + delete + " was successfully deleted");
                        errorLabel.setVisible(true);
                    }catch (NumberFormatException ex){
                        errorLabel.setText("Please insert a number for the id");
                        errorLabel.setVisible(true);
                    }catch (Exception ex){
                        errorLabel.setText("The product was not found");
                        errorLabel.setVisible(true);
                    }
                }
            }
            if(fromNewProduct == 1){
                Product existing = null;
                ProductBLL productBLL = new ProductBLL();
                if(!id.getText().isEmpty() && name.getText().isEmpty() && category.getText().isEmpty() && quantity.getText().isEmpty() && price.getText().isEmpty()){
                    try {
                        product.setId(Integer.parseInt(id.getText()));
                        existing = productBLL.findProductById(product.getId());
                        if(existing != null){
                            throw new IllegalArgumentException();
                        }
                    }catch(NumberFormatException ex){
                        errorLabel.setText("Please insert a valid id");
                        errorLabel.setVisible(true);
                    }catch(IllegalArgumentException ex){
                        errorLabel.setText("The id already exists");
                        errorLabel.setVisible(true);
                    }catch(NoSuchElementException ex){
                        name.setVisible(true);
                        nameLabel.setVisible(true);
                    }
                }else if(!id.getText().isEmpty() && !name.getText().isEmpty() && category.getText().isEmpty() && quantity.getText().isEmpty() && price.getText().isEmpty()){
                    product.setName(name.getText());
                    category.setVisible(true);
                    categoryLabel.setVisible(true);
                }else if(!id.getText().isEmpty() && !name.getText().isEmpty() && !category.getText().isEmpty() && quantity.getText().isEmpty() && price.getText().isEmpty()){
                    product.setCategory(category.getText());
                    quantity.setVisible(true);
                    quantityLabel.setVisible(true);
                }else if(!id.getText().isEmpty() && !name.getText().isEmpty() && !category.getText().isEmpty() && !quantity.getText().isEmpty() && price.getText().isEmpty()){
                    try {
                        product.setQuantity(Integer.parseInt(quantity.getText()));
                        price.setVisible(true);
                        priceLabel.setVisible(true);
                    }catch (NumberFormatException ex){
                        errorLabel.setText("Please insert a valid quantity");
                        errorLabel.setVisible(true);
                    }
                }else if(!id.getText().isEmpty() && !name.getText().isEmpty() && !category.getText().isEmpty() && !quantity.getText().isEmpty() && !price.getText().isEmpty()){
                    try {
                        product.setPrice(Integer.parseInt(price.getText()));
                        productBLL.insertProduct(product);
                        errorLabel.setText("The product was inserted");
                        errorLabel.setVisible(true);
                    }catch (NumberFormatException ex){
                        errorLabel.setText("Please insert a valid price");
                        errorLabel.setVisible(true);
                    }catch (IllegalArgumentException ex){
                        errorLabel.setText(ex.getMessage());
                        errorLabel.setVisible(true);
                    }
                }
            }
            if(fromEdit == 1){
                int update = 0;
                ProductBLL productBLL = new ProductBLL();
                if(!id.getText().isEmpty() && name.getText().isEmpty() && category.getText().isEmpty() && quantity.getText().isEmpty() && price.getText().isEmpty()){
                    try {
                        update = Integer.parseInt(id.getText());
                        product = productBLL.findProductById(update);
                        name.setVisible(true);
                        nameLabel.setVisible(true);
                        category.setVisible(true);
                        categoryLabel.setVisible(true);
                        quantity.setVisible(true);
                        quantityLabel.setVisible(true);
                        price.setVisible(true);
                        priceLabel.setVisible(true);
                    }catch(NumberFormatException ex){
                        errorLabel.setText("Please insert a valid id");
                        errorLabel.setVisible(true);
                    }catch(Exception ex){
                        errorLabel.setText("The product was not found");
                        errorLabel.setVisible(true);
                    }
                }else{
                    if(!name.getText().isEmpty()){
                        product.setName(name.getText());
                    }
                    if(!category.getText().isEmpty()){
                        product.setCategory(category.getText());
                    }
                    if(!quantity.getText().isEmpty()){
                        try {
                            product.setQuantity(Integer.parseInt(quantity.getText()));
                        }catch (NumberFormatException ex){
                            updateError = 1;
                            errorLabel.setText("Please insert a number");
                            errorLabel.setVisible(true);
                        }
                    }
                    if(!price.getText().isEmpty()){
                        try {
                            product.setPrice(Integer.parseInt(price.getText()));
                        }catch (NumberFormatException ex){
                            updateError = 1;
                            errorLabel.setText("Please insert a number");
                            errorLabel.setVisible(true);
                        }
                    }
                    if(updateError == 0) {
                        try {
                            productBLL.updateProduct(product);
                            errorLabel.setText("The product with id " + product.getId() + " was updated successfully");
                            errorLabel.setVisible(true);
                        } catch (IllegalArgumentException ex) {
                            errorLabel.setText(ex.getMessage());
                            errorLabel.setVisible(true);
                        }
                    }
                }

            }
        }else if(e.getSource().equals(clear)){
            newProduct.setDisable(false);
            editProduct.setDisable(false);
            deleteProduct.setDisable(false);
            viewProducts.setDisable(false);
            errorLabel.setText("");
            id.clear();
            id.setVisible(false);
            errorLabel.setVisible(false);
            idLabel.setVisible(false);
            name.clear();
            name.setVisible(false);
            nameLabel.setVisible(false);
            category.clear();
            category.setVisible(false);
            categoryLabel.setVisible(false);
            quantity.clear();
            quantity.setVisible(false);
            quantityLabel.setVisible(false);
            price.clear();
            priceLabel.setVisible(false);
            price.setVisible(false);
            fromDelete = 0;
            fromNewProduct = 0;
            fromEdit = 0;
            updateError = 0;
            productsTable.setVisible(false);
        }
    }
}

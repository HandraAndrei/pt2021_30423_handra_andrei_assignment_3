package presentation;

import bll.ClientBLL;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Client;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * This class is used for managing the Client operations:
 * Add new Client, Edit Client, Delete Client and View all Clients
 */

public class ClientsController {

    @FXML
    private Button newClient;
    @FXML
    private Button editClient;
    @FXML
    private Button deleteClient;
    @FXML
    private Button viewClients;
    @FXML
    private Label idLabel;
    @FXML
    private Label nameLabel;
    @FXML
    private Label ageLabel;
    @FXML
    private Label emailLabel;
    @FXML
    private Label townLabel;
    @FXML
    private TextField id;
    @FXML
    private TextField name;
    @FXML
    private TextField age;
    @FXML
    private TextField email;
    @FXML
    private TextField town;
    @FXML
    private Label errorLabel;
    @FXML
    private Button enter;
    @FXML
    private Button clear;
    @FXML
    public TableView<Client> clientsTable;

    /**
     * These fields are used to know when I press the Enter button, from
     * which operation we came ( The button that was pressed before )
     */
    private int fromDelete = 0;
    private int fromNewClient = 0;
    private int fromEdit = 0;

    private int initializeTable = 0;
    private int updateError = 0;
    Client client;

    /**
     * In this method the GUI for the Client operations is implemented
     * @param e is used to select between the available buttons to implement the required client operations
     */
    @FXML
    public void onButtonClicked(ActionEvent e){
        if(e.getSource().equals(newClient)){
            client = new Client();
            fromNewClient = 1;
            newClient.setDisable(true);
            editClient.setDisable(true);
            deleteClient.setDisable(true);
            viewClients.setDisable(true);
            id.setVisible(true);
            idLabel.setVisible(true);

        }else if(e.getSource().equals(editClient)){
            fromEdit = 1;
            newClient.setDisable(true);
            editClient.setDisable(true);
            deleteClient.setDisable(true);
            viewClients.setDisable(true);
            id.setVisible(true);
            idLabel.setVisible(true);


        }else if(e.getSource().equals(deleteClient)){
            fromDelete = 1;
            newClient.setDisable(true);
            editClient.setDisable(true);
            deleteClient.setDisable(true);
            viewClients.setDisable(true);
            idLabel.setVisible(true);
            id.setVisible(true);

        }else if(e.getSource().equals(viewClients)){
            clientsTable.getItems().clear();
            clientsTable.setEditable(true);
            ClientBLL clientBLL = new ClientBLL();
            List<Client> clients = clientBLL.findAllClients();
            if(initializeTable == 0) {
                clientsTable = clientBLL.getTableHeaders(clientsTable,clients,initializeTable);
                initializeTable = 1;
            }else if(initializeTable == 1){
                clientsTable = clientBLL.getTableHeaders(clientsTable,clients,initializeTable);
            }
            clientsTable.getColumns().get(0).setPrefWidth(50);
            clientsTable.getColumns().get(1).setPrefWidth(150);
            clientsTable.getColumns().get(2).setPrefWidth(50);
            clientsTable.getColumns().get(3).setPrefWidth(200);
            clientsTable.getColumns().get(4).setPrefWidth(150);
            clientsTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("id"));
            clientsTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("name"));
            clientsTable.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("age"));
            clientsTable.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("email"));
            clientsTable.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("town"));
            clientsTable.setVisible(true);
        }else if(e.getSource().equals(enter)){
            if(fromDelete == 1){
                int delete = 0;
                if(!id.getText().isEmpty()){
                    try {
                        delete = Integer.parseInt(id.getText());
                        ClientBLL clientBLL = new ClientBLL();
                        Client client1 = null;
                        client1 = clientBLL.findClientById(delete);
                        clientBLL.deleteClient(delete);
                        errorLabel.setText("The client with id " + delete + " was successfully deleted");
                        errorLabel.setVisible(true);
                    }catch (NumberFormatException ex){
                        errorLabel.setText("Please insert a number for the id");
                        errorLabel.setVisible(true);
                    }catch (Exception ex){
                        errorLabel.setText("The client was not found");
                        errorLabel.setVisible(true);
                    }
                }
            }
            if(fromNewClient == 1){
                Client existing = null;
                ClientBLL clientBLL = new ClientBLL();
                if(!id.getText().isEmpty() && name.getText().isEmpty() && age.getText().isEmpty() && email.getText().isEmpty() && town.getText().isEmpty()){
                  try {
                      client.setId(Integer.parseInt(id.getText()));
                      existing = clientBLL.findClientById(client.getId());
                      if(existing != null){
                          throw new IllegalArgumentException();
                      }
                  }catch(NumberFormatException ex){
                      errorLabel.setText("Please insert a valid id");
                      errorLabel.setVisible(true);
                  }catch(IllegalArgumentException ex){
                      errorLabel.setText("The id already exists");
                      errorLabel.setVisible(true);
                  }catch(NoSuchElementException ex){
                      name.setVisible(true);
                      nameLabel.setVisible(true);
                  }
                }else if(!id.getText().isEmpty() && !name.getText().isEmpty() && age.getText().isEmpty() && email.getText().isEmpty() && town.getText().isEmpty()){
                    client.setName(name.getText());
                    age.setVisible(true);
                    ageLabel.setVisible(true);
                }else if(!id.getText().isEmpty() && !name.getText().isEmpty() && !age.getText().isEmpty() && email.getText().isEmpty() && town.getText().isEmpty()){
                    try {
                        client.setAge(Integer.parseInt(age.getText()));
                        email.setVisible(true);
                        emailLabel.setVisible(true);
                    }catch(NumberFormatException ex){
                        errorLabel.setText("Please insert a valid age");
                        errorLabel.setVisible(true);
                        enter.setDisable(true);
                    }
                }else if(!id.getText().isEmpty() && !name.getText().isEmpty() && !age.getText().isEmpty() && !email.getText().isEmpty() && town.getText().isEmpty()){
                    client.setEmail(email.getText());
                   town.setVisible(true);
                   townLabel.setVisible(true);
                }else if(!id.getText().isEmpty() && !name.getText().isEmpty() && !age.getText().isEmpty() && !email.getText().isEmpty() && !town.getText().isEmpty()){
                    client.setTown(town.getText());
                    try {
                        clientBLL.insertClient(client);
                        errorLabel.setText("The client was inserted");
                        errorLabel.setVisible(true);
                    }catch (IllegalArgumentException ex){
                        errorLabel.setText(ex.getMessage());
                        errorLabel.setVisible(true);
                    }
                }

            }
            if(fromEdit == 1){
                int update = 0;
                ClientBLL clientBLL = new ClientBLL();
                if(!id.getText().isEmpty() && name.getText().isEmpty() && age.getText().isEmpty() && email.getText().isEmpty() && town.getText().isEmpty()){
                    try {
                        update = Integer.parseInt(id.getText());
                        client = clientBLL.findClientById(update);
                        name.setVisible(true);
                        nameLabel.setVisible(true);
                        age.setVisible(true);
                        ageLabel.setVisible(true);
                        email.setVisible(true);
                        emailLabel.setVisible(true);
                        town.setVisible(true);
                        townLabel.setVisible(true);
                    }catch(NumberFormatException ex){
                        errorLabel.setText("Please insert a valid id");
                        errorLabel.setVisible(true);
                    }catch(Exception ex){
                        errorLabel.setText("The client was not found");
                        errorLabel.setVisible(true);
                    }
                }else{
                    if(!name.getText().isEmpty()){
                        client.setName(name.getText());
                    }
                    if(!age.getText().isEmpty()){
                        try {
                            client.setAge(Integer.parseInt(age.getText()));
                        }catch (NumberFormatException ex){
                            updateError = 1;
                            errorLabel.setText("Please insert a number");
                            enter.setDisable(true);
                            errorLabel.setVisible(true);
                        }
                    }
                    if(!email.getText().isEmpty()){
                        client.setEmail(email.getText());
                    }
                    if(!town.getText().isEmpty()){
                        client.setTown(town.getText());
                    }
                    if(updateError == 0) {
                        try {
                            clientBLL.updateClient(client);
                            errorLabel.setText("The client with id " + client.getId() + " was updated successfully");
                            errorLabel.setVisible(true);
                        } catch (IllegalArgumentException ex) {
                            errorLabel.setText(ex.getMessage());
                            errorLabel.setVisible(true);
                        }
                    }
                }
            }
        }else if(e.getSource().equals(clear)){
            newClient.setDisable(false);
            editClient.setDisable(false);
            deleteClient.setDisable(false);
            viewClients.setDisable(false);
            errorLabel.setText("");
            id.setText("");
            errorLabel.setVisible(false);
            idLabel.setVisible(false);
            id.setVisible(false);
            name.clear();
            age.clear();
            email.clear();
            town.clear();
            name.setVisible(false);
            age.setVisible(false);
            email.setVisible(false);
            town.setVisible(false);
            nameLabel.setVisible(false);
            ageLabel.setVisible(false);
            townLabel.setVisible(false);
            emailLabel.setVisible(false);
            enter.setDisable(false);
            fromDelete = 0;
            fromNewClient = 0;
            fromEdit = 0;
            updateError = 0;
            clientsTable.setVisible(false);
        }
    }

}

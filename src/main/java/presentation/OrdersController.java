package presentation;

import bll.ClientBLL;
import bll.OrdersBLL;
import bll.ProductBLL;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.StringConverter;
import model.Client;
import model.Orders;
import model.Product;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

/**
 * This class is used for managing the Product operations:
 * Add new Product, Edit Product, Delete Product and View all Products
 */

public class OrdersController {
    @FXML
    private Button newOrder;
    @FXML
    private Button viewOrders;
    @FXML
    private Button clear;
    @FXML
    private Button enter;
    @FXML
    private Button proceed;
    @FXML
    private Label errorLabel;
    @FXML
    TableView<Orders> ordersTable;
    @FXML
    ComboBox<Client> clientsCombo;
    @FXML
    ComboBox<Product> productsCombo;
    @FXML
    ComboBox<Integer> quantityCombo;
    private int initializeTable = 0;
    Orders order;
    Client client;
    Product product;
    int quantity;

    /**
     * This method is used to create the headers for the PDF bill table
     * @param table is a parameter used for creating the pdf table header
     */

    private void addTableHeader(PdfPTable table) {
        Stream.of("Name","Product","Quantity","Price")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    /**
     * This method is used to populate the PDF bill's table with the specific values
     * @param table is the PDF's table
     * @param order is the Orders object for which we make the bill
     */
    private void addRows(PdfPTable table,Orders order) {
        table.addCell(order.getClientName());
        table.addCell(order.getProductName());
        table.addCell(String.valueOf(order.getQuantity()));
        table.addCell(String.valueOf(order.getPrice()));
    }

    /**
     * This method is used to make the bill
     * @param order is the Orders object for which we make the bill
     */
    public void makeBill(Orders order){
        Document document = new Document();
        Random random = new Random();
        String orderString = String.valueOf(random.nextInt(100000-1)+1);
        try{
            PdfWriter.getInstance(document,new FileOutputStream("Order"+orderString+".pdf"));
            document.open();
            Font font1 = FontFactory.getFont(FontFactory.COURIER_BOLD,20, BaseColor.BLACK);
            Paragraph paragraph;
            paragraph = new Paragraph();
            paragraph.setAlignment(Paragraph.ALIGN_CENTER);
            paragraph.setFont(font1);
            paragraph.add("Order confirmed!");
            document.add(paragraph);
            paragraph = new Paragraph(" ");
            document.add(paragraph);
            PdfPTable table = new PdfPTable(4);
            addTableHeader(table);
            addRows(table,order);
            document.add(table);
            paragraph = new Paragraph(" ");
            document.add(paragraph);
            paragraph = new Paragraph();
            paragraph.setAlignment(Paragraph.ALIGN_RIGHT);
            paragraph.add("Delivery Cost: " + order.getShippingCost());
            document.add(paragraph);
            paragraph = new Paragraph(" ");
            document.add(paragraph);
            paragraph = new Paragraph();
            paragraph.setAlignment(Paragraph.ALIGN_RIGHT);
            paragraph.add("The total sum is: " + order.getTotal());
            document.add(paragraph);
            paragraph = new Paragraph(" ");
            document.add(paragraph);
            paragraph = new Paragraph();
            paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            LocalDate date = LocalDate.now();
            random = new Random();
            int minDelivery = 2;
            int maxDelivery = 6;
            LocalDate deliveryDate = date.plusDays(random.nextInt(maxDelivery - minDelivery) + minDelivery);
            paragraph.add("The delivery is expected on: " + deliveryDate);
            document.add(paragraph);
            paragraph = new Paragraph(" ");
            document.add(paragraph);
            paragraph = new Paragraph();
            paragraph.setAlignment(Paragraph.ALIGN_RIGHT);
            paragraph.add("Current date: " + date);
            document.add(paragraph);

            document.close();

        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (DocumentException e){
            e.printStackTrace();
        }
    }

    /**
     * In this method the GUI for the Order operations is implemented
     * @param e is used to select between the available buttons to implement the required orders operations
     */

    public void onButtonClicked(ActionEvent e){
        if(e.getSource().equals(newOrder)) {
            proceed.setDisable(false);
            clear.setDisable(false);
            order = new Orders();
            newOrder.setDisable(true);
            enter.setDisable(true);
            ClientBLL clientBLL = new ClientBLL();
            List<Client> clients = clientBLL.findAllClients();
            clientsCombo.getItems().addAll(clients);

            ProductBLL productBLL = new ProductBLL();
            List<Product> products = productBLL.findAllProducts();
            for(Product product:products){
                if(product.getQuantity() != 0){
                    productsCombo.getItems().add(product);
                }
            }
            clientsCombo.setVisible(true);
            productsCombo.setVisible(true);

        }else if(e.getSource().equals(proceed)){
            proceed.setDisable(true);
            quantity = productsCombo.getValue().getQuantity();
            for (int i = 1; i <= quantity; i++) {
                quantityCombo.getItems().add(i);
            }
            quantityCombo.setVisible(true);
            enter.setDisable(false);
        }else if(e.getSource().equals(viewOrders)) {
            clear.setDisable(false);
            ordersTable.getItems().clear();
            ordersTable.setEditable(true);
            OrdersBLL ordersBLL = new OrdersBLL();
            List<Orders> orders = ordersBLL.findAllOrders();
            if (initializeTable == 0) {
                ordersTable = ordersBLL.getTableHeaders(ordersTable,orders,initializeTable);
                initializeTable = 1;
            }else if(initializeTable == 1){
                ordersTable = ordersBLL.getTableHeaders(ordersTable,orders,initializeTable);
            }
            ordersTable.getColumns().get(0).setPrefWidth(50);
            ordersTable.getColumns().get(1).setPrefWidth(200);
            ordersTable.getColumns().get(2).setPrefWidth(200);
            ordersTable.getColumns().get(3).setPrefWidth(100);
            ordersTable.getColumns().get(4).setPrefWidth(100);
            ordersTable.getColumns().get(5).setPrefWidth(100);
            ordersTable.getColumns().get(6).setPrefWidth(100);

            ordersTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("id"));
            ordersTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("productName"));
            ordersTable.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("clientName"));
            ordersTable.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("quantity"));
            ordersTable.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("price"));
            ordersTable.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("shippingCost"));
            ordersTable.getColumns().get(6).setCellValueFactory(new PropertyValueFactory<>("total"));
            ordersTable.setVisible(true);
        }else if(e.getSource().equals(enter)){
            enter.setDisable(true);
            proceed.setDisable(true);
            order = new Orders();
            client = new Client();
            product = new Product();
            client = clientsCombo.getValue();
            product = productsCombo.getValue();
            quantity = quantityCombo.getValue();
            order.setClientName(client.getName());
            order.setProductName(product.getName());
            order.setPrice(product.getPrice());
            order.setQuantity(quantity);
            if(product.getPrice() > 300){
                order.setShippingCost(0);
            }else{
                order.setShippingCost(15);
            }
            order.setTotal(order.getPrice()*order.getQuantity() + order.getShippingCost());
            OrdersBLL ordersBLL = new OrdersBLL();
            ordersBLL.insertOrder(order);
            errorLabel.setText("Order placed successfully");
            errorLabel.setVisible(true);
            ProductBLL productBLL = new ProductBLL();
            product.setQuantity(product.getQuantity()-order.getQuantity());
            productBLL.updateProduct(product);

            makeBill(order);

        }else if(e.getSource().equals(clear)){
            ordersTable.setVisible(false);
            clientsCombo.setVisible(false);
            productsCombo.setVisible(false);
            quantityCombo.setVisible(false);
            errorLabel.setText("");
            errorLabel.setVisible(false);
            newOrder.setDisable(false);
            enter.setDisable(true);
            proceed.setDisable(true);
            clear.setDisable(true);
            productsCombo.getItems().clear();
            clientsCombo.getItems().clear();
            quantityCombo.getItems().clear();

        }
    }
}

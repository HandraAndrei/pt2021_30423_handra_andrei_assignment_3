package presentation;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * This class Controller is used to make the three windows
 * for the three main types: Clients, Products and Orders operations
 */

public class Controller {
    @FXML
    private Button clients;
    @FXML
    private Button orders;
    @FXML
    private Button products;

    /**
     * <p>
     *     The method is used to select between the clients, products or order operations
     * </p>
     * @param e is used for the button which can be pressed
     */
    @FXML
    public void onButtonClicked(ActionEvent e) {
        if(e.getSource().equals(clients)){
            try{
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/clients.fxml"));
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root,1000,1000));
                stage.setTitle("Clients Management");
                stage.show();

            }catch(IOException exception){
                exception.printStackTrace();
            }

        }else if(e.getSource().equals(orders)){
            try{
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/orders.fxml"));
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root,1000,900));
                stage.setTitle("Orders Management");
                stage.show();
            }catch (IOException ex){
                ex.printStackTrace();
            }
        }else if(e.getSource().equals(products)){
            try{
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/products.fxml"));
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root,1000,1000));
                stage.setTitle("Products Management");
                stage.show();
            }catch (IOException ex){
                ex.printStackTrace();
            }
        }
    }
}
